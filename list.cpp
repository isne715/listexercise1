#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if (tail == 0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	Node* tmp = head;
	while (tmp != tail)
	{
		tmp = tmp->next;
	}
	tmp->next = new Node(el, 0);
	tail = tmp->next;
}
char List::popHead()
{
	char el = head->data;
	Node* tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	Node* tmp = head;
	if (head == tail) {
		return head->data;
		head = tail = 0;
	}
	else {
		while (tmp->next != tail)
		{
			tmp = tmp->next;
		}
		char el = tmp->next->data;
		tail = tmp;
		return el;
	}
}
bool List::search(char el)
{
	Node* tmp = head;
	bool check = false;
	while (check != true)
	{
		tmp = tmp->next;
		if (tmp->data == el) {
			check = true;
		}
		if (tmp == tail) {
			break;
		}
	}
	return check;
}
void List::reverse()
{
	List Reverse;
	Node* tmp = head;
	while (tmp != tail)
	{
		Reverse.pushToHead(tmp->data);
		tmp = tmp->next;
		if (tmp == tail) {
			Reverse.pushToHead(tmp->data);
		}
	}
	Reverse.print();
}

void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node* tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}

void List::checkPalindrome() 
{
	List Reverse;									//create reverse list.
	Node* tmp = head;
	while (tmp != tail)
	{
		Reverse.pushToHead(tmp->data);
		tmp = tmp->next;
		if (tmp == tail) {
			Reverse.pushToHead(tmp->data);
		}
	}

	Node* ptr1 = head;
	Node* ptr2 = Reverse.head;
	bool check = true;
	while (ptr1 != tail && ptr2 != tail) {			//compare reverse with true list.
		if (ptr1->data != ptr2->data) {
			check = false;
		}
		ptr1 = ptr1->next;
		ptr2 = ptr2->next;
	}
	if (check == true) {
		cout << "This linklist is palimdrome.";
	}
	else {
		cout << "This linklist is not palimdrome.";
	}
}