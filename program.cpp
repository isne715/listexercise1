#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	mylist.pushToHead('v');
	mylist.pushToHead('e');
	mylist.pushToHead('k');
	mylist.pushToTail('e');
	mylist.pushToTail('l');

	List yourlist;
	yourlist.pushToHead('v');
	yourlist.pushToHead('e');
	yourlist.pushToHead('l');
	yourlist.pushToTail('e');
	yourlist.pushToTail('l');

	cout << "Print mylist : "; mylist.print();cout << endl;
	mylist.checkPalindrome();
	cout << endl << "Print yourlist: "; yourlist.print(); cout << endl;
	yourlist.checkPalindrome();
	cout << endl << endl << "-----mylist proof-----";
	cout << endl << "Reverse : "; mylist.reverse();
	cout << endl << "Poptail : "<< mylist.popTail() << endl;
	cout << "Print(after poptail) : "; mylist.print();
	cout << endl << "Search(out list) : " << mylist.search('j');
	cout << endl << "Search(in list) : " << mylist.search('e') << endl;


	
	

	//TO DO! Write a program that tests your list library - the code should take characters, push them onto a list, 
	//- then reverse the list to see if it is a palindrome!

}